
<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<div class="container">
    <div>
         <?php if (session()->get("success")) : ?>
            <div class="alert alert-success">
                <?= session()->get("success") ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($errors)) : ?>
            <div class="alert alert-danger">
                <?php foreach ($errors as $field => $error) : ?>
                    <p><?= $error ?></p>
                <?php endforeach ?>
            </div>
        <?php endif ?>
    </div>
    <?= form_open(site_url("alumnosController/actualitza/$id"))?>
        <div class="mb-3">
            <?= form_label('NIA','nia',['class'=>'form-label'])?>
            <?= form_input('NIA', set_value('NIA',$alumne['NIA']),['class'=>'form-control','placeholder'=> "NIA", "data-sb-validations"=>"required", 'id'=>"nia"])?>
        </div>
        <div class="mb-3">
            <?= form_label('Nom','nombre',['class'=>'form-label'])?>
            <?= form_input('nombre', set_value('nombre',$alumne['nombre']),['class'=>'form-control','placeholder'=> "Nom", "data-sb-validations"=>"required", 'id'=>"nombre"])?>
        </div>
        <div class="mb-3">
            <?= form_label('1er cognom','apellido1',['class'=>'form-label'])?>
            <?= form_input('apellido1', set_value('apellido1',$alumne['apellido1']),['class'=>'form-control','placeholder'=> "1er cognom", "data-sb-validations"=>"required", 'id'=>"apellido1"])?>
        </div>
        <div class="mb-3">
            <?= form_label('2ºn cognom','apellid2',['class'=>'form-label'])?>
            <?= form_input('apellido2', set_value('apellido2',$alumne['apellido2']),['class'=>'form-control','placeholder'=> "2on cognom", 'id'=>"apellido2"])?>
        </div>
        <div class="mb-3">
            <?= form_label('NIF/NIE','nif',['class'=>'form-label'])?>
            <?= form_input('nif', set_value('nif',$alumne['nif']),['class'=>'form-control','placeholder'=> "nif", "data-sb-validations"=>"required", 'id'=>"nif"])?>
        </div>
        <div class="mb-3">
            <?= form_label('Data naixement','fecha_nac',['class'=>'form-label'])?>
            <?= form_input('fecha_nac', set_value('fecha_nac',$alumne['fecha_nac']),['class'=>'form-control','placeholder'=> "Data naixement", "data-sb-validations"=>"required", 'id'=>"fecha_nac"])?>
        </div>
        <div class="mb-3">
            <?= form_label('E-mail','email',['class'=>'form-label'])?>
            <?= form_input('email', set_value('email',$alumne['email']),['class'=>'form-control','placeholder'=> "E-mail", "data-sb-validations"=>"required", 'id'=>"email"])?>
        </div>
        
        <div class="mb-3">
            <?= form_submit('Lliurar','Lliurar',["class"=>"btn btn-primary"]) ?>
            <a href="<?= site_url('alumnosController') ?>" class="btn btn-primary">Tornar</a>
        </div>
    <?= form_close()?>
</div>

<?= $this->endSection() ?>
