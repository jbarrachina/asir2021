<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<div class="mb-3">
    <a href="<?= site_url('grupsController') ?>" class="btn btn-primary">Tornar</a>
    <a href="<?= site_url('alumnosController/actualitza') ?>" class="btn btn-primary">Afegir</a>
</div>
<table class="table table-striped table-condensed tabla">
    <thead>
        <th>1er Apellido</th>
        <th>2º Apellido</th>
        <th>Nombre</th>
        <td>Acciones</th>
    </thead>
    <tbody>
    <?php foreach ($alumnes as $alumne): ?>
        <tr>
            <td><?= $alumne['apellido1'] ?></td>
            <td><?= $alumne['apellido2'] ?></td>
            <td><?= $alumne['nombre'] ?></td>
            <td><a href="<?=site_url('alumnosController/actualitza/'.$alumne['id'])?>">Editar</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>    
</table>
 <?= $this->endSection() ?>

 
