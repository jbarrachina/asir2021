<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\AlumnesModel;

/**
 * Description of AlumnosController
 *
 * @author depinf
 */
class AlumnosController extends BaseController{
        
    public function index(){
        $datos['titol'] = "Llistat de tots els Alumnes";
        $datos['alumnes'] = $this->alumnes
                ->orderBy('apellido1, apellido2')
                ->findAll();
        echo view('alumnes/llista',$datos);
    }
    
    public function grup($grup='2CFSS'){
        $datos['titol'] = "Llistat d'Alumnes del Grupo $grup";
        
        $datos['alumnes'] = $this->alumnes
                ->select("CONCAT(alumnos.apellido1,' ',alumnos.apellido2,', ',alumnos.nombre) as nombre_completo")
                ->select('alumnos.id, alumnos.NIA, alumnos.email, matricula.estado')
                ->join('matricula','matricula.NIA=alumnos.NIA','LEFT')
                ->where(['matricula.grupo'=>$grup])
                ->orderBy('nombre_completo')
                ->findAll();
        /*echo '<pre>';
        print_r($datos);
        echo '</pre>';*/
        echo view('alumnes/llistagrup',$datos);
    }
    
    /***************************************************************************
     * si $nia es NULL s'afegirà un nou registre, en cas contrari s'editarà
     **************************************************************************/
    public function actualitza($id = NULL){
        helper(['form']);
        $data['id'] = $id;
        $action = $id===NULL ? 'Afegir un nou' : 'Editar un';
        $data['titol'] = "$action alumne";
        if ($this->request->getMethod() == "post") { //viene de un formulario
            $data['alumne'] = $this->request->getPost();  //cogemos los valores de los campos
            if ($id!==NULL) {
                $data['alumne']['id'] = $id;   //estamos editando y recuperamos el $id del parámetro
            }
            if ($this->alumnes->save($data['alumne']) === false) {
                    $data['errors'] = $this->alumnes->errors();
                    return view('alumnes/afegir', $data);
            } else {
                    return redirect()->to(site_url('alumnosController/afegir'));
            }
        } else { //else viene de una URL
            $data['alumne'] = $this->alumnes->getAlumne(['id'=>$id]);
            if ($data['alumne']==FALSE){ //el alumno buscado no se encuentra
               $data ['id'] = NULL; 
               $data['alumne'] = [
                "NIA" => "",
                "nombre" => "",
                "apellido1" => "",
                "apellido2" => "",
                "nif" => "",
                "fecha_nac" => "",
                "email" => "",
            ];
            } 
            return view("alumnes/actualitza",$data);  
        }   
    }
    
    public function prova(){
        var_dump($this->alumnes->getAlumne(['NIA'=>10003062]));
    }
}
