<?php

namespace App\Controllers;

use App\Models\GruposModel;
use CodeIgniter\Controller;

class GruposController extends Controller
{
    //mostrar todo el alumnado
    public function index()
    {
        $datos['titol'] = "Llista de grups";
        $grups = new GruposModel(); //creamos el modelo
        $datos['grups'] = $grups->findAll(); //lo guardamos en el array asociativo que pasaremos a la vista
        echo view('grups/llista', $datos); //mostramos los datos a través de una vista
    }
    
   
}
