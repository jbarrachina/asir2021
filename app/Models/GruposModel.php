<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of AlumnosModel
 *
 * @author jose
 */
class GruposModel extends Model {
    protected $table = 'grupos';
    protected $returnType = 'object';
}
