<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of AlumnosModel
 *
 * @author jose
 */
class AlumnosModel extends Model {
    protected $table = 'alumnos';
}
