<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use CodeIgniter\Model;
/**
 * Description of AlumneModel
 *
 * @author depinf
 */
class AlumnesModel extends Model {
    protected $table = 'alumnos';
    protected $primaryKey = 'id';
    protected $allowedFields        = [
		"NIA", 
		"nombre", 
		"apellido1",
                "apellido2",
                "fecha_nac",
                "nif",
                "email"
    ];
    //Validation
    protected $validationRules = [
                "NIA" => "required|exact_length[8]|integer|trim",
                "nombre" => "required|min_length[3]|trim",
                "apellido1" => "required|min_length[2]|trim",
                "email" => "required|valid_email|trim",
                "fecha_nac" => "valid_date[Y-m-d]",
                "nif" => "nifValidation",
                ];
    protected $validationMessages = [
                "NIA" => 
                    ["required" => "El NIA no puede estar en blanco",
                     "exact_length[8]" => "El NIA debe tener 8 dígitos",
                     "integer" => "El NIA sólo puede contener dígitos"
                    ],
                "nombre" => 
                    ["required" => "El nombre no puede estar en blanco",
                     "min_length[2]" => "El nombre debe tener al menos dos letras",
                     "alpha_space" => "El nombre sólo puede contener letras y espacios"   
                    ],    
                "apellido1" => 
                    ["required" => "El 1er apellido no puede estar en blanco",
                     "min_length[3]" => "El apellido debe tener más de dos letras",
                     "alpha_space" => "El apellido sólo puede contener letras y espacios"   
                    ],
                "nif"=>
                    ["nifValidation" => "Formato de NIF/NIE erróneo",
                    ]
                ];
    /***************************************************************************
     * Recuperar un únic alumne, en cas de no trobar-lo retornarà FALSE
     **************************************************************************/
    public function getAlumne($where){
        return $this->where($where)->first();
    }
}
